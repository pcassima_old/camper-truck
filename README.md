# Camper truck

## Concept & idea

A small off-road, 4x4, truck converted to a camper. With a trailer carrying either two (dirt) bikes or a small car. To use when visiting places where the truck won’t fit or be practical. A city for example. The trailer can also fit extra supplies and utilities, such as a generator, extra water & fuel and spare parts.

## Truck

- GAZ-66 with (tall) radio-box
- Trailer (with same wheels & ground clearance) for extra utilities, generator and motorbikes/small car.

## Requirements

### Electrical system

- 12/24V system
- Inverter for 110/220V (for devices that can’t be converted to low voltage DC)
- Accept 220V as an input, for charging and supply (from a generator for example)
- Solar panels, with a fold out section so they offer shade, and while stationary extra power.
- AC (heating and cooling) + air filter

### Water system

- Sunboiler for heating (as well as an electrical one)
- Large supply tank (for multiple days without refilling)
- if possible, a water recycling system

### Interior

- 140 x 200 cm bed (two person bed)
- desk / table
- tool storage
- clothes storage
- fridge
- sample / souvenir storage

### Communications

- Powerful radio capable of working on multiple frequencies
- Internet (for uploading videos, etc)

### Other

- powerful winch
- more than one spare wheel
- jack, strong enough to lift the truck
- Enlarge fuel tanks (extra reach)
- Swap out motor (less pollution and more efficient further reach)
- lightweight materials (driver’s license restrictions etc)
- durable (bumpers, etc )
- proper insulation (sound and heat/cold)



